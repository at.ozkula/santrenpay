-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `event_detail`;
CREATE TABLE `event_detail` (
  `ideventdetail` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `event_deskripsi` text NOT NULL,
  PRIMARY KEY (`ideventdetail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `event_donasi`;
CREATE TABLE `event_donasi` (
  `ideventdonasi` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `event_value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ideventdonasi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `event_kategori`;
CREATE TABLE `event_kategori` (
  `ideventkategori` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`ideventkategori`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `event_kategori` (`ideventkategori`, `event_name`) VALUES
(1,	'Pembayaran'),
(2,	'Donasi');

DROP TABLE IF EXISTS `event_payment`;
CREATE TABLE `event_payment` (
  `ideventpayment` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `event_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`ideventpayment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `log_transaction`;
CREATE TABLE `log_transaction` (
  `idlogtransaction` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(16) NOT NULL,
  `transaction_type` int(2) NOT NULL,
  `amount` varchar(10) NOT NULL DEFAULT '',
  `nohp` varchar(15) NOT NULL DEFAULT '',
  `status_id` int(2) NOT NULL,
  `timelog` datetime NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`idlogtransaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `merchant`;
CREATE TABLE `merchant` (
  `idmerchant` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_kategori_id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL DEFAULT '',
  `deskripsi` text NOT NULL,
  `alamat` text NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`idmerchant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `merchant_kategori`;
CREATE TABLE `merchant_kategori` (
  `idkategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`idkategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `merchant_kategori` (`idkategori`, `kategori`) VALUES
(1,	'Pesantren'),
(2,	'MiniMarket'),
(3,	'Kantin'),
(4,	'Donasi / Pil Rejeki');

DROP TABLE IF EXISTS `sp_event`;
CREATE TABLE `sp_event` (
  `idevent` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_kategori_id` int(11) NOT NULL,
  `event_nama` varchar(50) NOT NULL DEFAULT '',
  `merchant_id` int(11) NOT NULL,
  PRIMARY KEY (`idevent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(50) NOT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `status` (`idstatus`, `status_name`) VALUES
(1,	'User Baru Belum Diverifikasi'),
(2,	'Transaksi Gagal'),
(3,	'Transaksi Gagal Saldo Kurang'),
(4,	'Menerima Dana'),
(5,	'Mengirim Dana');

DROP TABLE IF EXISTS `transaction_type`;
CREATE TABLE `transaction_type` (
  `idtransactiontype` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_name` varchar(50) NOT NULL,
  PRIMARY KEY (`idtransactiontype`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `transaction_type` (`idtransactiontype`, `transaction_name`) VALUES
(1,	'Kirim Uang'),
(2,	'Pembayaran');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `nohp` varchar(15) NOT NULL,
  `balance` int(10) NOT NULL,
  `token` varchar(60) NOT NULL,
  `authpin` int(6) NOT NULL,
  `loginTime` datetime NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user_detail`;
CREATE TABLE `user_detail` (
  `iduser_detail` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `verifyemail` varchar(11) NOT NULL,
  `otp` int(6) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`iduser_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2018-07-19 07:36:52
