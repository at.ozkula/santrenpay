-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sp_log_transaction`;
CREATE TABLE `sp_log_transaction` (
  `idlogtransaction` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(16) NOT NULL,
  `transaction_type` int(2) NOT NULL,
  `amount` varchar(10) NOT NULL DEFAULT '',
  `sender` varchar(15) NOT NULL DEFAULT '',
  `reciever` varchar(15) NOT NULL DEFAULT '',
  `status_id` int(2) NOT NULL,
  `timelog` datetime NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`idlogtransaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sp_merchant`;
CREATE TABLE `sp_merchant` (
  `idmerchant` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `deskripsi` text NOT NULL,
  `alamat` text NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`idmerchant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sp_merchant_kategori`;
CREATE TABLE `sp_merchant_kategori` (
  `idkategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`idkategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sp_merchant_kategori` (`idkategori`, `kategori`) VALUES
(1,	'Pesantren'),
(2,	'MiniMarket'),
(3,	'Kantin'),
(4,	'Pil Rejeki');

DROP TABLE IF EXISTS `sp_status`;
CREATE TABLE `sp_status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(50) NOT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sp_status` (`idstatus`, `status_name`) VALUES
(1,	'User Baru Belum Diverifikasi'),
(2,	'Transaksi Berhasil'),
(3,	'Transaksi Gagal'),
(4,	'Transaksi Gagal Saldo Kurang');

DROP TABLE IF EXISTS `sp_transaction_type`;
CREATE TABLE `sp_transaction_type` (
  `idtransactiontype` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_name` varchar(50) NOT NULL,
  PRIMARY KEY (`idtransactiontype`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sp_transaction_type` (`idtransactiontype`, `transaction_name`) VALUES
(1,	'Kirim Uang'),
(2,	'Pembayaran');

DROP TABLE IF EXISTS `sp_user`;
CREATE TABLE `sp_user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `nohp` varchar(15) NOT NULL,
  `balance` int(10) NOT NULL,
  `token` varchar(60) NOT NULL,
  `authpin` int(6) NOT NULL,
  `loginTime` datetime NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sp_user_auth`;
CREATE TABLE `sp_user_auth` (
  `iduser_auth` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `open_id_type` varchar(10) NOT NULL,
  `open_id` text NOT NULL,
  PRIMARY KEY (`iduser_auth`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sp_user_detail`;
CREATE TABLE `sp_user_detail` (
  `iduser_detail` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `otp` int(6) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`iduser_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2018-06-12 04:10:08
