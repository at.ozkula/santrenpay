<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|    example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|    https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|    $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|    $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|    $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:    my-controller/index    -> my_controller/index
|        my-controller/my-method    -> my_controller/my_method
 */
$route['default_controller']   = 'home';
$route['404_override']         = '';
$route['translate_uri_dashes'] = false;

// Route SantrenPay
$route['registerStep1'] = 'user/registerStep1';
$route['registerStep2'] = 'user/registerStep2';
$route['registerStep3'] = 'user/registerStep3';
$route['registerStep4'] = 'user/registerStep4';

$route['login']           = 'user/login';
$route['checkbalance']    = 'user/checkBalance';
$route['checkreciever']   = 'user/checkreciever';
$route['checkauthpin']    = 'user/checkauthpin';
$route['log_transaction'] = 'user/log_transaction';
// payment
$route['sendmoney']       = 'payment/sendmoney';
$route['pay_event']       = 'payment/pay_event';
$route['pay_tagihan']     = 'payment/pay_tagihan';
$route['payment_invoice'] = 'payment/payment_invoice';
$route['saldorequest']    = 'payment/saldoRequest';
// Merchant
$route['merchant/list']          = 'merchant/list';
$route['merchant/list_event']    = 'merchant/list_event';
$route['merchant/list_kategori'] = 'merchant/list_kategori';
$route['merchant/event']         = 'merchant/event';
$route['merchant/event_detail']  = 'merchant/event_detail';
// Payment
$route['payment/list_pesantren'] = 'payment/list_pesantren';
$route['payment/list_tagihan']   = 'payment/list_tagihan';
// Wali SAntri
$route['wali_santri/check_anak']  = 'wali_santri/check_anak';
$route['wali_santri/list_anak']   = 'wali_santri/list_anak';
$route['wali_santri/simpan_anak'] = 'wali_santri/simpan_anak';
$route['wali_santri/hapus_anak']  = 'wali_santri/hapus_anak';
