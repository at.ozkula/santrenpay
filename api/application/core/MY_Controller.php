<?php
require APPPATH . '/libraries/REST_Controller.php';

class MY_Controller extends REST_Controller
{
    // Check For Normalize number remove 62 and 0 and +62
    public function NumberOnly($nohp)
    {
        // Check zero Number
        if (substr($nohp, 0, 2) == '62' || substr($nohp, 0, 1) == '+') {
            $country_code = "+62";
            return preg_replace("/^\+?{$country_code}/", '', $nohp);
        } else if (substr($nohp, 0, 2) == '08') {
            return preg_replace('~^[0\D]++|\D++~', '', $nohp);
        } else {
            return $nohp;
        }
    }
    public function checkTokenExist($token)
    {
        $this->load->model('user_model', 'user_model');
        return $this->user_model->_check_token($token);
    }
}
