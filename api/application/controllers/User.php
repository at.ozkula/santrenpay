<?php
/**
 * API FOR SANTRENPAY UserModel
 * Register Step 1 -> input Nama,Email,Nohp -> return Email & Token
 * Register Step 2 -> input verifyCode, Token -> get Email From Server
 * Register Step 3 -> Input Password, token -> return true
 * Register Step 4 -> Input authpin, token -> return true
 *
 */
class User extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $emptyField = "Data Tidak Dikirim Semua, Silahkan ISI Semua Data";
    }
    public function registerStep1_post()
    {
        // Step 1Input Data Return email and token
        $nama  = $this->post('nama');
        $nohp  = $this->NumberOnly($this->post('nohp'));
        $email = $this->post('email');

        $parameterSend = array(
            'nama'  => $nama,
            'nohp'  => $nohp,
            'email' => $email,
            'token' => $this->_generate_key(),
        );
        $resultDB = $this->user_model->registerStep1($parameterSend);
        $this->response($resultDB, REST_Controller::HTTP_OK);

    }
    public function registerStep2_post()
    {
        // Step 2 Input Email and TOken to get Response From Server
        $verifyCode = $this->post('verifyCode');
        $token      = $this->post('token');

        $parameterSend = array(
            'verifyCode' => $verifyCode,
            'token'      => $token,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->user_model->registerStep2($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function registerStep3_post()
    {
        // Step 3 Insert Password For User
        $token         = $this->post('token');
        $password      = $this->post('password');
        $parameterSend = array(
            'password' => $password,
            'token'    => $token,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->user_model->registerStep3($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function registerStep4_post()
    {
        $token         = $this->post('token');
        $authpin       = $this->post('authpin');
        $parameterSend = array(
            'authpin' => $authpin,
            'token'   => $token,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->user_model->registerStep4($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function login_post()
    {
        $nohp          = $this->NumberOnly($this->post('nohp'));
        $authpin       = $this->post('authpin');
        $parameterSend = array(
            'nohp'    => $nohp,
            'authpin' => $authpin,
            'token'   => $this->_generate_key(),
        );
        $resultDB = $this->user_model->login($parameterSend);
        $this->response($resultDB, REST_Controller::HTTP_OK);
    }
    public function checkbalance_get()
    {
        $nohp          = $this->NumberOnly($this->get('nohp'));
        $token         = $this->get('token');
        $parameterSend = array(
            'nohp'  => $nohp,
            'token' => $token,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->user_model->checkbalance($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }

    }
    public function checkreciever_get()
    {
        $nohp          = $this->NumberOnly($this->get('nohp'));
        $parameterSend = array(
            'reciever' => $nohp,
        );
        $resultDB = $this->user_model->__auth_reciever($parameterSend);
        $this->response($resultDB, REST_Controller::HTTP_OK);
    }
    public function checkauthpin_post()
    {
        $nohp          = $this->NumberOnly($this->get('nohp'));
        $authpin       = $this->post('authpin');
        $token         = $this->post('token');
        $parameterSend = array(
            'nohp'    => $nohp,
            'authpin' => $authpin,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->user_model->checkauthpin($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function log_transaction_post()
    {
        $nohp          = $this->NumberOnly($this->post('nohp'));
        $token         = $this->post('token');
        $parameterSend = array(
            'nohp' => $nohp,
        );

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->user_model->log_transaction($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    private function _generate_key()
    {
        // Generate a random salt
        $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

        // If an error occurred, then fall back to the previous method
        if ($salt === false) {
            $salt = hash('sha256', time() . mt_rand());
        }
        $new_key = substr($salt, 0, config_item('rest_key_length'));

        return $new_key;
    }

}
