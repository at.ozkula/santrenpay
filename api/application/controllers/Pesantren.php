<?php
/**
 * API FOR SANTRENPAY
 */
class Pesantren extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pesantren_model');
    }
    public function list_pesantren_post()
    {
        $token = $this->post('token');

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->pesantren_model->list_pesantren();
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function list_tagihan_post()
    {

        $token         = $this->post('token');
        $pesantren_id  = $this->post('pesantren_id');
        $nis           = $this->post('nis');
        $parameterSend = array(
            'token'        => $token,
            'pesantren_id' => $pesantren_id,
            'nis'          => $nis,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->pesantren_model->list_tagihan($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
}
