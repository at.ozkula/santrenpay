<?php
/**
 * Controller untuk memanajemen Wali Santri
 */
class Wali_santri extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('wali_santri_model');
    }
    public function check_anak_post()
    {
        $token         = $this->post('token');
        $pesantren_id  = $this->post('pesantren_id');
        $nis           = $this->post('nis');
        $parameterSend = array(
            'token'        => $token,
            'pesantren_id' => $pesantren_id,
            'nis'          => $nis,
        );

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->wali_santri_model->check_anak($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function list_anak_post()
    {
        $token      = $this->post('token');
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->wali_santri_model->list_anak($token);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function simpan_anak_post()
    {
        $token         = $this->post('token');
        $nis           = $this->post('nis');
        $pesantren_id  = $this->post('pesantren_id');
        $parameterSend = array(
            'token'        => $token,
            'nis'          => $nis,
            'pesantren_id' => $pesantren_id,
        );

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->wali_santri_model->simpan_anak($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function hapus_anak_post()
    {
        $token         = $this->post('token');
        $nis           = $this->post('nis');
        $pesantren_id  = $this->post('pesantren_id');
        $parameterSend = array(
            'token' => $token,
            'nis'   => $nis,
            'pesantren_id' => $pesantren_id,
        );

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->wali_santri_model->hapus_anak($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
}
