<?php
/**
 * API FOR SANTRENPAY
 */
class Payment extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_model');
    }
    public function sendmoney_post()
    {
        $token         = $this->post('token');
        $sender        = $this->NumberOnly($this->post('sender'));
        $reciever      = $this->NumberOnly($this->post('reciever'));
        $amount        = $this->post('amount');
        $authpinsender = $this->post('authpinsender');

        $parameterSend = array(
            'token'         => $token,
            'sender'        => $sender,
            'reciever'      => $reciever,
            'amount'        => $amount,
            'authpinsender' => $authpinsender,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->payment_model->sendmoney($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }

    }
    public function pay_event_post()
    {
        $token         = $this->post('token');
        $event_id      = $this->post('event_id');
        $sender        = $this->NumberOnly($this->post('sender'));
        $authpinsender = $this->post('authpinsender');
        $amount        = $this->post('amount');

        $parameterSend = array(
            'token'         => $token,
            'event_id'      => $event_id,
            'sender'        => $sender,
            'authpinsender' => $authpinsender,
            'amount'        => $amount,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->payment_model->payment_event($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }

    }
    public function pay_tagihan_post()
    {
        // Bayar Tagihan dari Santren
        $token         = $this->post('token');
        $idpembayaran  = $this->post('idpembayaran');
        $sender        = $this->NumberOnly($this->post('sender'));
        $authpinsender = $this->post('authpinsender');

        $parameterSend = array(
            'token'         => $token,
            'idpembayaran'  => $idpembayaran,
            'sender'        => $sender,
            'authpinsender' => $authpinsender,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->payment_model->payment_tagihan($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function payment_invoice_post()
    {
        // Bayar Tagihan dari Santren
        $token         = $this->post('token');
        $idpembayaran  = $this->post('idpembayaran');

        $parameterSend = array(
            'token'         => $token,
            'idpembayaran'  => $idpembayaran,
        );
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->payment_model->payment_invoice($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    public function saldoRequest_post()
    {
        $token  = $this->post('token');
        $no_ref = $this->post('no_ref');

        $parameterSend = array(
            'token'  => $token,
            'no_ref' => $no_ref,
        );

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->payment_model->saldoRequest($parameterSend);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
}
