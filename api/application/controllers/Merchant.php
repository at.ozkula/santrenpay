<?php
/**
 * Merchant Digunakan untuk mengatur Kantin,MiniMarket,piL Rejeki
 */
class Merchant extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('merchant_model');
    }
    // list merchant
    public function list_post()
    {
        $token    = $this->post('token');
        $kategori = $this->post('kategori');

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            if (!empty($kategori)) {
                $resultDB = $this->merchant_model->list_merchant_kategori($kategori);
            } else {
                $resultDB = $this->merchant_model->list_merchant();
            }
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    // List Event all
    public function list_event_post()
    {
        $token      = $this->post('token');
        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->merchant_model->list_event();
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    // Check Content didalam Merchant
    public function event_post()
    {
        $token       = $this->post('token');
        $merchant_id = $this->post('merchant_id');

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->merchant_model->event_merchant($merchant_id);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    // Check detail Event Merchant
    public function event_detail_post()
    {
        $token    = $this->post('token');
        $event_id = $this->post('event_id');

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->merchant_model->event_detail($event_id);
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    // Get List Kategori
    public function list_kategori_post()
    {
        $token = $this->post('token');

        $checkToken = $this->checkTokenExist($token);
        if ($checkToken == true) {
            $resultDB = $this->merchant_model->list_kategori();
            $this->response($resultDB, REST_Controller::HTTP_OK);
        } else {
            $resultDB['status'] = "Token Invalid";
            $this->response($resultDB, REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

}
