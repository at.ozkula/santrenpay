<?php
/**
 * Payment Model for Santren Pay 4Juni2018
 * Untuk Payment pertama kali dilakukan yaitu Check Balance -> Deduct balance(potong balance)-> Move balance (memindahkan ke saldo penerima), semua dilakukan berurutank
 */
class Payment_model extends CI_Model
{
    public function sendmoney($parameter)
    {
        $this->load->model('user_model', 'user_model');
        $this->load->model('log_model', 'log_model');

        $token         = $parameter['token'];
        $sender        = '0' . $parameter['sender'];
        $reciever      = '0' . $parameter['reciever'];
        $amount        = $parameter['amount'];
        $authpinsender = $parameter['authpinsender'];

        $CheckUser        = $this->user_model->_auth_sendmoney($parameter);
        $senderResponse   = $CheckUser['sender']['code'];
        $recieverResponse = $CheckUser['reciever']['code'];
        if ($senderResponse == 200 && $recieverResponse == 200) {
            if ($this->_checkBalance($parameter) == 'cukup') {
                // Notif Deduct
                $ddBalance          = $this->_deductBalance($parameter);
                $deductBalanceNotif = "Mengirim sebesar " . $parameter['amount'] . ' Ke 0' . $parameter['reciever'];
                $this->log_model->_log_transaction(1, $parameter['amount'], '0' . $parameter['sender'], 6, $deductBalanceNotif);
                // Notif Move balance
                $mmBalance        = $this->_MoveBalance($parameter);
                $moveBalanceNotif = "Menerima sebesar " . $parameter['amount'] . ' Dari 0' . $parameter['sender'];
                $this->log_model->_log_transaction(1, $parameter['amount'], '0' . $parameter['reciever'], 5, $moveBalanceNotif);
                //
                $result['result'] = "Berhasil Mengirim sebesar " . $parameter['amount'];
                $result['status'] = REST_Controller::HTTP_OK;
            } else {
                $response         = "Saldo Anda Kurang";
                $result['result'] = $response;
                $result['status'] = REST_Controller::HTTP_ACCEPTED;
                $this->log_model->_log_transaction(1, $parameter['amount'], $sender, 3, $response);
            }
        } else if ($senderResponse == 401 && $recieverResponse == 200) {
            $response         = "Pengirim Salah Data";
            $result['result'] = $response;
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
            $this->log_model->_log_transaction(1, $parameter['amount'], $sender, 2, $response);
        } else if ($senderResponse == 200 && $recieverResponse == 401) {
            $response         = "Penerima Salah Data";
            $result['result'] = $response;
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
            $this->log_model->_log_transaction(1, $parameter['amount'], $sender, 2, $response);
        }
        return $result;

    }
    // Event
    public function payment_event($parameter)
    {
        $this->load->model('user_model', 'user_model');
        $this->load->model('log_model', 'log_model');
        $this->load->model('merchant_model', 'merchant_model');

        $event = $parameter['event_id'];
        // Get Event Detail to get Reciever and Amount
        $event_gedetail = $this->merchant_model->event_detail($event);

        $getEventKategori = $event_gedetail['result']['ideventkategori'];

        if ($getEventKategori == 1) {
            // Pembayaran
            $amount   = $event_gedetail['result']['nominal'];
            $reciever = substr($event_gedetail['result']['nohp'], 1);
        } else if ($getEventKategori == 2) {
            // Donasi
            $amount   = $parameter['amount'];
            $reciever = substr($event_gedetail['result']['nohp'], 1);
        }

        $ToMethodOther = array(
            'token'         => $parameter['token'],
            'sender'        => $parameter['sender'],
            'amount'        => $amount,
            'reciever'      => $reciever,
            'authpinsender' => $parameter['authpinsender'],
            'event'         => $event,
        );
        // End Event Detail Get reciever and Amount
        $CheckUser = $this->user_model->_auth_sendmoney($ToMethodOther);

        $senderResponse   = $CheckUser['sender']['code'];
        $recieverResponse = $CheckUser['reciever']['code'];
        $sender           = '0' . $parameter['sender'];

        if ($senderResponse == 200 && $recieverResponse == 200) {
            if ($this->_checkBalance($ToMethodOther) == 'cukup') {
                $ddBalance = $this->_deductBalance($ToMethodOther);
                $mmBalance = $this->_MoveBalance($ToMethodOther);
                if ($getEventKategori == 2) {
                    $MoveToDonate = $this->_MoveToDonate($ToMethodOther);
                }
                $result['result'] = "Berhasil Mengirim sebesar " . $ToMethodOther['amount'];
                $result['status'] = REST_Controller::HTTP_OK;
                $this->log_model->_log_transaction(1, $ToMethodOther['amount'], $sender, 3, $result['result']);
            } else {
                $response         = "Saldo Anda Kurang";
                $result['result'] = $response;
                $result['status'] = REST_Controller::HTTP_ACCEPTED;
                $this->log_model->_log_transaction(1, $ToMethodOther['amount'], $sender, 3, $response);
            }
        } else if ($senderResponse == 401 && $recieverResponse == 200) {
            $response         = "Pengirim Salah Data";
            $result['result'] = $response;
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
            $this->log_model->_log_transaction(1, $ToMethodOther['amount'], $sender, 2, $response);
        } else if ($senderResponse == 200 && $recieverResponse == 401) {
            $response         = "Penerima Salah Data";
            $result['result'] = $response;
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
            $this->log_model->_log_transaction(1, $ToMethodOther['amount'], $sender, 2, $response);
        }
        return $result;

    }
    // Tagihan
    public function payment_tagihan($parameter)
    {
        $this->load->model('user_model', 'user_model');
        $this->load->model('log_model', 'log_model');
        $this->load->model('pesantren_model', 'pesantren_model');

        $idpembayaran = $parameter['idpembayaran'];
        // Get Payment Detail to get Reciever and Amount
        $pembayaran_detail = $this->pesantren_model->detail_pembayaran($idpembayaran);

        $amount        = $pembayaran_detail['biaya'];
        $reciever      = substr($pembayaran_detail['nohp'], 1);
        $idpembayaran  = $pembayaran_detail['idpembayaran'];
        $nama_tagihan  = $pembayaran_detail['nama_tagihan'];
        $ToMethodOther = array(
            'token'         => $parameter['token'],
            'sender'        => $parameter['sender'],
            'amount'        => $amount,
            'reciever'      => $reciever,
            'authpinsender' => $parameter['authpinsender'],
            'idpembayaran'  => $idpembayaran,
        );
        // Send To CheckGet reciever and Amount
        $CheckUser        = $this->user_model->_auth_sendmoney($ToMethodOther);
        $senderResponse   = $CheckUser['sender']['code'];
        $recieverResponse = $CheckUser['reciever']['code'];
        $sender           = '0' . $parameter['sender'];

        if ($senderResponse == 200 && $recieverResponse == 200) {
            if ($this->_checkBalance($ToMethodOther) == 'cukup') {
                $ddBalance        = $this->_deductBalance($ToMethodOther);
                $mmBalance        = $this->_MoveBalance($ToMethodOther);
                $result['result'] = "Berhasil Membayar sebesar " . $ToMethodOther['amount'] . " Untuk " . $nama_tagihan;
                $result['status'] = REST_Controller::HTTP_OK;
                $this->log_model->_log_transaction(1, $ToMethodOther['amount'], $sender, 3, $result['result']);
                $this->gantiStatusTagihan($ToMethodOther);
            } else {
                $response         = "Saldo Anda Kurang";
                $result['result'] = $response;
                $result['status'] = REST_Controller::HTTP_ACCEPTED;
                $this->log_model->_log_transaction(1, $ToMethodOther['amount'], $sender, 3, $response);
            }
        } else if ($senderResponse == 401 && $recieverResponse == 200) {
            $response         = "Pengirim Salah Data";
            $result['result'] = $response;
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
            $this->log_model->_log_transaction(1, $ToMethodOther['amount'], $sender, 2, $response);
        } else if ($senderResponse == 200 && $recieverResponse == 401) {
            $response         = "Penerima Salah Data";
            $result['result'] = $response;
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
            $this->log_model->_log_transaction(1, $ToMethodOther['amount'], $sender, 2, $response);
        }
        return $result;
    }
    public function gantiStatusTagihan($parameter)
    {
        $PesantrenDB             = $this->load->database('pesantren', true);
        $PemabayaranChangeStatus = array(
            'status_bayar' => 1,
        );
        $PesantrenDB->where('idpembayaran', $parameter['idpembayaran']);
        $PesantrenDB->update('pembayaran', $PemabayaranChangeStatus);
    }
    public function payment_invoice($parameter)
    {

        $PesantrenDB  = $this->load->database('pesantren', true);
        $idpembayaran = $parameter['idpembayaran'];

        $QueryListInvoice = "SELECT * FROM pembayaran py
                            LEFT JOIN anak a ON a.`nis` = py.`nis_anak_id` AND a.`pesantren_id` = py.`pesantren_id`
                            LEFT JOIN bayar b ON b.`idbayar` = py.`bayar_id`
                            LEFT JOIN pesantren ps ON ps.idpesantren = py.`pesantren_id`
                            LEFT JOIN jenis_kelamin jk ON jk.`idjenis_kelamin` = b.`jenis_kelamin`
                            LEFT JOIN waktu_tagihan wt ON wt.`idwaktu_tagihan` = py.`waktu_tagihan_id`
                            LEFT JOIN jenis_tagihan jt ON jt.`idjenis_tagihan` = b.`jenis_tagihan`
                             WHERE py.idpembayaran = ?";

        $CheckResult = $PesantrenDB->query($QueryListInvoice, array($idpembayaran))->result();
        if (!empty($CheckResult)) {
            foreach ($CheckResult as $subarray) {
                $listTagihan['nama_anak']    = $subarray->nama;
                $listTagihan['nis']          = $subarray->nis;
                $listTagihan['pesantren']    = $subarray->nama_santren;
                $listTagihan['pesantren_id'] = $subarray->pesantren_id;
                $listTagihan['detail'][0]    = array(
                    'idpembayaran'   => $subarray->idpembayaran,
                    'tagihan'        => $subarray->nama_tagihan,
                    'detail_tagihan' => $subarray->detail,
                    'jenis_tagihan'  => $subarray->jenis_tagihan,
                    'biaya'          => $subarray->biaya,
                    'status_bayar'   => $subarray->status_bayar,
                );
            }
            $result['result'] = $listTagihan;
            $result['status'] = REST_Controller::HTTP_OK;
        } else {
            $result['result'] = "No Record Show";
            $result['status'] = REST_Controller::HTTP_NO_CONTENT;
        }
        return $result;
    }
    private function _checkBalance($parameter)
    {
        $checkBalance  = "SELECT balance FROM user WHERE nohp = ?";
        $resultNominal = $this->db->query($checkBalance, array('0' . $parameter['sender']))->result_array();
        if ($resultNominal[0]['balance'] >= $parameter['amount']) {
            $result = "cukup";
        } else if ($resultNominal[0]['balance'] < $parameter['amount']) {
            $result = "kurang";
        }
        return $result;
    }
    private function _deductBalance($parameter)
    {
        // get the Balance Sender
        $GetLastBalance = "SELECT balance FROM user WHERE nohp = ?";
        $resultBalance  = $this->db->query($GetLastBalance, array('0' . $parameter['sender']))->result_array();
        // Lha ini Pengurangan e / deduct
        $deductBalance = $resultBalance[0]['balance'] - $parameter['amount'];
        // UPdate ke database
        $dataChange = array(
            'balance' => $deductBalance,
        );
        $this->db->where('nohp', '0' . $parameter['sender']);
        $this->db->update('user', $dataChange);

    }
    private function _MoveBalance($parameter)
    {
        // Add Balance Because Add Amount
        // get the Balance Reciever
        $GetLastBalance = "SELECT balance FROM user WHERE nohp = ?";
        $resultBalance  = $this->db->query($GetLastBalance, array('0' . $parameter['reciever']))->result_array();
        // Lha ini Tambahe Add
        $addBalance = $resultBalance[0]['balance'] + $parameter['amount'];
        // UPdate ke database
        $dataChange = array(
            'balance' => $addBalance,
        );
        $this->db->where('nohp', '0' . $parameter['reciever']);
        $this->db->update('user', $dataChange);

    }
    private function _MoveToDonate($parameter)
    {
        // get the Balance Reciever
        $GetLastBalance = "SELECT event_value FROM event_donasi WHERE event_id = ?";
        $resultBalance  = $this->db->query($GetLastBalance, array($parameter['event']))->result_array();
        // Lha ini Tambahe Add
        $donateValue = $resultBalance[0]['event_value'] + $parameter['amount'];
        // UPdate ke database
        $dataChange = array(
            'event_value' => $donateValue,
        );
        $this->db->where('event_id', $parameter['event']);
        $this->db->update('event_donasi', $dataChange);
    }
    // Method to request saldo to Payment Server
    //
    public function saldoRequest($parameter)
    {
        $this->load->model('log_model', 'log_model');
        $passData = array(
            "token-key" => 'sp4y',
            "no_ref"    => $parameter['no_ref'],
        );
        $url = 'http://192.168.1.2/payment/api/getSaldo';

        $saldoResult = (array) json_decode($this->CURL($url, $passData));
        $saldoResult = $saldoResult['saldo'];
        // Get Nomor HP yg Punya Balance
        $getNoHP         = "SELECT nohp,balance FROM user WHERE token = ?";
        $resultNoHP      = $this->db->query($getNoHP, array($parameter['token']))->result_array();
        $recieverSaldo   = substr($resultNoHP[0]['nohp'], 1);
        $totalSaldoadded = $resultNoHP[0]['balance'] + $saldoResult;
        $parameterToAdd  = array(
            'reciever' => $recieverSaldo,
            'amount'   => $saldoResult,
        );
        $this->_MoveBalance($parameterToAdd);
        if ($saldoResult == 0) {
            $result['result'] = 'Saldo Already Claimed Please Contact US';
            $result['status'] = REST_Controller::HTTP_NO_CONTENT;
        } else {
            $result['result'] = 'Saldo bertambah Rp.' . $saldoResult . ' Total Saldo Anda Rp.' . $totalSaldoadded;
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
        }
        $this->log_model->_log_transaction(3, $result['result'], 'SantrenPay', 4, $result['result']);
        return $result;

    }
    public function CURL($_url, $_param)
    {

        $postData = '';
        //create name value pairs seperated by &
        foreach ($_param as $k => $v) {
            $postData .= $k . '=' . $v . '&';
        }
        rtrim($postData, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

}
