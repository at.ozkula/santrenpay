<?php
/**
 * Model Koneksi dengan database pesantren
 * 23 Juli 2018
 */
class Pesantren_model extends CI_Model
{
    public function list_pesantren()
    {
        $PesantrenDB      = $this->load->database('pesantren', true);
        $QueryListSantren = "SELECT * FROM pesantren";
        $CheckResult      = $PesantrenDB->query($QueryListSantren)->result_array();

        $result['result'] = $CheckResult;
        $result['status'] = REST_Controller::HTTP_OK;

        return $result;

    }
    public function list_tagihan($parameter)
    {
        $PesantrenDB  = $this->load->database('pesantren', true);
        $nis_anak_id  = $parameter['nis'];
        $pesantren_id = $parameter['pesantren_id'];

        $QueryListSantren = "SELECT * FROM pembayaran py
                            LEFT JOIN anak a ON a.`nis` = py.`nis_anak_id` AND a.`pesantren_id` = py.`pesantren_id`
                            LEFT JOIN bayar b ON b.`idbayar` = py.`bayar_id`
                            LEFT JOIN pesantren ps ON ps.`idpesantren` = py.`pesantren_id`
                            LEFT JOIN jenis_kelamin jk ON jk.`idjenis_kelamin` = b.`jenis_kelamin`
                            LEFT JOIN waktu_tagihan wt ON wt.`idwaktu_tagihan` = py.`waktu_tagihan_id`
                            LEFT JOIN jenis_tagihan jt ON jt.`idjenis_tagihan` = b.`jenis_tagihan`
                             WHERE a.nis = ? AND a.pesantren_id = ?";

        $CheckResult = $PesantrenDB->query($QueryListSantren, array($nis_anak_id, $pesantren_id))->result();
        if (!empty($CheckResult)) {
            foreach ($CheckResult as $subarray) {
                $listTagihan['nama_anak']    = $subarray->nama;
                $listTagihan['nis']          = $subarray->nis;
                $listTagihan['pesantren']    = $subarray->nama_santren;
                $listTagihan['pesantren_id'] = $subarray->pesantren_id;
                $listTagihan['detail'][]     = array(
                    'idpembayaran'   => $subarray->idpembayaran,
                    'tagihan'        => $subarray->nama_tagihan,
                    'detail_tagihan' => $subarray->detail,
                    'jenis_tagihan'  => $subarray->jenis_tagihan,
                    'biaya'          => $subarray->biaya,
                    'status_bayar'   => $subarray->status_bayar,
                );
            }
            $result['result'] = $listTagihan;
            $result['status'] = REST_Controller::HTTP_OK;
        } else {
            $result['result'] = "No Record Show";
            $result['status'] = REST_Controller::HTTP_NO_CONTENT;
        }
        return $result;
    }
    public function detail_pembayaran($parameter)
    {
        $PesantrenDB         = $this->load->database('pesantren', true);
        $QueryListPembayaran = "SELECT * FROM pembayaran py
                                LEFT JOIN bayar b ON b.`idbayar` = py.`bayar_id`
                                LEFT JOIN pesantren ps ON ps.`idpesantren` = py.`pesantren_id`
                                WHERE idpembayaran = ?";
        $listBayar = $PesantrenDB->query($QueryListPembayaran, array($parameter))->result_array();
        if (empty($listBayar)) {
            $resultDetailBayar = "zeroResult";
        } else {
            $merchantID   = $listBayar[0]['merchant_id'];
            $biaya        = $listBayar[0]['biaya'];
            $pesantren_id = $listBayar[0]['pesantren_id'];
            $idpembayaran = $listBayar[0]['idpembayaran'];
            $nama_tagihan = $listBayar[0]['nama_tagihan'];

            $QueryGetNoHp = "SELECT nohp FROM merchant m
                         LEFT JOIN user u ON u.iduser = m.user_id
                         WHERE m.user_id = ?";
            $ResultFromMerchant = $this->db->query($QueryGetNoHp, array($merchantID))->result_array()[0];

            $resultDetailBayar = array(
                'merchant_id'  => $merchantID,
                'biaya'        => $biaya,
                'pesantren_id' => $pesantren_id,
                'idpembayaran' => $idpembayaran,
                'nama_tagihan' => $nama_tagihan,
                'nohp'         => $ResultFromMerchant['nohp'],
            );
        }
        return $resultDetailBayar;
    }
}
