<?php
/** For SantrenPay
 * User Model to Interact
 */
class User_model extends CI_Model
{
    //  _   _
    // | | | |
    // | | | |___  ___ _ __
    // | | | / __|/ _ \ '__|
    // | |_| \__ \  __/ |
    //  \___/|___/\___|_|

    public function registerStep1($parameter)
    {
        $nama          = $parameter['nama'];
        $nohp          = '0' . $parameter['nohp'];
        $email         = $parameter['email'];
        $token         = $parameter['token'];
        $getVerifyCode = substr($token, 0, 6);

        $CheckResult = $this->_check_user_register($parameter);

        if ($CheckResult > 0) {
            $result['result'] = 'Data Sudah Ada';
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
        } else {
            
            if ($this->sendEmail($parameter) == true) {
            $dataUser = array(
                'nohp'  => $nohp,
                'token' => $token,
            );
            $this->db->insert('user', $dataUser);
            $lastUserID = $this->db->insert_id();
            $dataDetail = array(
                'nama'        => $nama,
                'user_id'     => $lastUserID,
                'email'       => $email,
                'verifyemail' => $getVerifyCode,
            );
            $this->db->insert('user_detail', $dataDetail);
            
            // Must Approve
            $result['result'] = array('email' => $email, 'token' => $token);
            $result['status'] = array('status' => REST_Controller::HTTP_CREATED, 'message' => 'Step 1 Complete');
            
            } else {
                $this->db->trans_rollback();
                $result['result'] = array('email' => $email, 'token' => $token);
                $result['status'] = array('status' => REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, 'message' => 'Step Fails');
            }
        }
        return $result;

    }
    public function registerStep2($parameter)
    {
        $verifyCode = $parameter['verifyCode'];
        $token      = $parameter['token'];

        $queryRegister2 = "SELECT * FROM user
                           LEFT JOIN user_detail ON user.iduser = user_detail.user_id
                           WHERE verifyemail = ? AND user.token = ?";
        $resultRegister2 = $this->db->query($queryRegister2, array($verifyCode, $token))->num_rows();
        if ($resultRegister2 > 0) {
            $successqueryRegister2 = "UPDATE user_detail LEFT JOIN user ON user.iduser = user_detail.user_id
                            SET verifyemail = 'success' , status = 2 WHERE user.token = ?";
            $successresultRegister3 = $this->db->query($successqueryRegister2, array($token));
            $result['result']       = 'Step 2 Complete, Verify Success';
            $result['status']       = array('status' => REST_Controller::HTTP_ACCEPTED, 'message' => 'Step 2 Complete, Verify Success');
        } else {
            $result['result'] = 'Verify Code Salah atau Token Salah';
            $result['status'] = array('status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message'                          => 'Step 2 Error, Verify Fails');
        }

        return $result;
    }
    public function registerStep3($parameter)
    {
        $password = $parameter['password'];
        $token    = $parameter['token'];

        $queryRegister3 = "UPDATE user_detail LEFT JOIN user ON user.iduser = user_detail.user_id
                            SET password = ? , status = 3 WHERE user.token = ?";
        $resultRegister3  = $this->db->query($queryRegister3, array($password, $token));
        $result['result'] = 'Step 3 Complete, Password Success';
        $result['status'] = array('status' => REST_Controller::HTTP_ACCEPTED, 'message' => 'Step 3 Complete, Password Success');
        return $result;
    }
    public function registerStep4($parameter)
    {
        $authpin = $parameter['authpin'];
        $token   = $parameter['token'];

        $queryRegister4 = "UPDATE user LEFT JOIN user_detail ON user.iduser = user_detail.user_id
                            SET authpin = ? , user_detail.status = 4 WHERE user.token = ?";
        $resultRegister3 = $this->db->query($queryRegister4, array($authpin, $token));

        $result['result'] = 'Step 4 Complete, AuthPin Success';
        $result['status'] = array('status' => REST_Controller::HTTP_ACCEPTED, 'message' => 'Step 4 Complete, AuthPin Success');
        return $result;
    }
    public function sendEmail($parameter)
    {
        $token         = $parameter['token'];
        $getVerifyCode = substr($token, 0, 6);

        $sender_email    = 'santrenpay@satsetbatbet.com';
        $sender_password = 'EbaGR9UD';
        $receiver_email  = $parameter['email'];
        $username        = $parameter['nama'];
        $subject         = "Verifikasi Email SantrenPay";
        $Pesan_kode      = "Masukkan kode berikut pada aplikasi SantrenPay <br><h2>" . $getVerifyCode . "</h2>";
        $message         = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'> <head> <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/> <title>ProUI Email Template - Welcome</title> <meta name='viewport' content='width=device-width'/> <style type='text/css'> @media only screen and (max-width: 550px), screen and (max-device-width: 550px){body[yahoo] .buttonwrapper{background-color: transparent !important;}body[yahoo] .button{padding: 0 !important;}body[yahoo] .button a{background-color: #888888; padding: 15px 25px !important;}}@media only screen and (min-device-width: 601px){.content{width: 600px !important;}.col387{width: 387px !important;}}</style> </head> <body bgcolor='#e51400' style='margin: 0; padding: 0;' yahoo='fix'><table align='center' border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse; width: 100%; max-width: 600px;' class='content'> <tr> <td align='center' bgcolor='#888888' style='padding: 20px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 36px; font-weight: bold;'> <img src='http://satsetbatbet.com/santrenpay/logo.png' width='350' alt='Launch Icon' width='200' height='200' style='display:block;'/><br/> Santren Pay </td></tr><tr> <td align='center' bgcolor='#ffffff' style='padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;'> <b>" . $Pesan_kode . "</b> </td></tr><tr> <td align='center' bgcolor='#dddddd' style='padding: 15px 10px 15px 10px; color: #555555; font-family: Arial, sans-serif; font-size: 12px; line-height: 18px;'> <b>SantrenPay.</b><br/>Semarang, Indonesia</td></tr></table></body></html>";

        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'mx1.hostinger.co.id',
            'smtp_port' => 587,
            'smtp_user' => $sender_email,
            'smtp_pass' => $sender_password,
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1',
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($sender_email, "SantrenPay");
        $this->email->to($receiver_email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function login($parameter)
    {
        $nohp    = '0' . $parameter['nohp'];
        $authpin = $parameter['authpin'];
        $token   = $parameter['token'];

        $CheckResult = $this->_check_user($parameter);

        if ($CheckResult[0]['count'] > 0) {
            $UpdateToken = array(
                'token'     => $token,
                'loginTime' => date('Y-m-d H:i:s'),
            );
            $this->db->where('nohp', $nohp);
            $this->db->update('user', $UpdateToken);

            $ShowData   = "SELECT nohp,balance,token,loginTime,nama,email FROM user LEFT JOIN user_detail ON user.iduser = user_detail.user_id WHERE nohp = ? AND authpin = ?";
            $ShowResult = $this->db->query($ShowData, array($nohp, $authpin))->result_array();

            $result['result'] = $ShowResult;
            $result['status'] = REST_Controller::HTTP_OK;
        } else {
            $result['result'] = 'Gagal Login';
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
        }
        return $result;
    }
    public function checkbalance($parameter)
    {
        $nohp  = '0' . $parameter['nohp'];
        $token = $parameter['token'];

        $BalanceCheck  = "SELECT balance FROM user WHERE nohp = ? AND token = ?";
        $resultBalance = $this->db->query($BalanceCheck, array($nohp, $token))->result_array();
        $CheckResult   = $this->db->query($BalanceCheck, array($nohp, $token))->num_rows();

        if ($CheckResult > 0) {
            $result['balance'] = $resultBalance[0]['balance'];
            $result['status']  = REST_Controller::HTTP_OK;
        } else {
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
        }

        return $result;
    }

    public function _check_user($parameter)
    {
        // If user Exist Result will be 1 and if no user result 0
        $nohp    = '0' . $parameter['nohp'];
        $token   = $parameter['token'];
        $authpin = $parameter['authpin'];

        $CheckCount  = "SELECT COUNT(*) AS 'count' FROM user WHERE nohp = ? AND authpin = ?";
        $CheckResult = $this->db->query($CheckCount, array($nohp, $authpin))->result_array();

        return $CheckResult;
    }
    public function _check_user_register($parameter)
    {
        // If user Exist Result will be 1 and if no user result 0
        $nohp  = '0' . $parameter['nohp'];
        $email = $parameter['email'];

        $CheckCount  = "SELECT * FROM user LEFT JOIN user_detail ON user.iduser = user_detail.user_id WHERE nohp = ? OR email = ?";
        $CheckResult = $this->db->query($CheckCount, array($nohp, $email))->num_rows();

        return $CheckResult;
    }
    public function _check_token($token)
    {

        $CheckCount  = "SELECT COUNT(*) AS 'count' FROM user WHERE token = ?";
        $CheckResult = $this->db->query($CheckCount, array($token))->result_array();
        $CheckResult = $CheckResult[0]['count'];
        if ($CheckResult > 0) {
            return true;
        } else {
            return false;
        }
    }
//  ______                                    __
    // |   __ \.---.-.--.--.--------.-----.-----.|  |_
    // |    __/|  _  |  |  |        |  -__|     ||   _|
    // |___|   |___._|___  |__|__|__|_____|__|__||____|
    //               |_____|
    public function __auth_reciever($parameter)
    {

        $reciever = '0' . $parameter['reciever'];

        $CheckCount = "SELECT COUNT(*) AS 'count'
                       FROM user WHERE nohp = ?";
        $CheckData = "SELECT nohp,nama FROM user
                       LEFT JOIN user_detail ON user_detail.user_id = user.iduser
                       WHERE nohp = ?";
        $CheckResult = $this->db->query($CheckCount, array($reciever))->result_array();
        $ResultData  = $this->db->query($CheckData, array($reciever))->result_array();
        
        if ($CheckResult[0]['count'] == 1) {
            $result = array('message' => 'Reciever True', 'code' => 200, 'data' => $ResultData[0]);
        } else {
            $result = array('message' => 'Reciever False', 'code' => 401, 'data' => null);
        }
        return $result;
    }
    public function __auth_sender($parameter)
    {

        $sender  = '0' . $parameter['sender'];
        $token   = $parameter['token'];
        $authpin = $parameter['authpinsender'];

        $CheckCount = "SELECT COUNT(*) AS 'count'
                       FROM user WHERE nohp = ? AND token = ? AND authpin = ?";
        $CheckResult = $this->db->query($CheckCount, array($sender, $token, $authpin))->result_array();

        if ($CheckResult[0]['count'] == 1) {
            $result = array('message' => 'Sender True', 'code' => 200);
        } else {
            $result = array('message' => 'Sender False', 'code' => 401);
        }
        return $result;
    }
// Authentication For Sending Money
    public function _auth_sendmoney($parameter)
    {
        $sender             = '0' . $parameter['sender'];
        $reciever           = '0' . $parameter['reciever'];
        $token              = $parameter['token'];
        $authpin            = $parameter['authpinsender'];
        $result['sender']   = $this->__auth_sender($parameter);
        $result['reciever'] = $this->__auth_reciever($parameter);

        return $result;
    }
    public function checkauthpin($parameter)
    {

        $nohp    = '0' . $parameter['nohp'];
        $authpin = $parameter['authpin'];

        $CheckCount  = "SELECT COUNT(*) AS 'count' FROM user WHERE authpin = ? AND nohp = ?";
        $CheckResult = $this->db->query($CheckCount, array($authpin, $nohp))->result_array();
        $CheckResult = $CheckResult[0]['count'];
        if ($CheckResult > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function log_transaction($parameter)
    {

        $nohp = '0' . $parameter['nohp'];

        $CheckResult = "SELECT idlogtransaction,transaction_id,transaction_type,user.nohp,status_id,reason FROM log_transaction
                        LEFT JOIN user ON user.nohp = log_transaction.nohp
                        WHERE user.nohp = ? AND status_id IN ('5','6')";
        $ShowResult = $this->db->query($CheckResult, array($nohp))->result_array();
        $CheckRow   = $this->db->query($CheckResult, array($nohp))->num_rows();

        if ($CheckRow > 0) {
            $result['result'] = $ShowResult;
            $result['status'] = REST_Controller::HTTP_OK;
        } else {
            $result['result'] = "No Data Show";
            $result['status'] = REST_Controller::HTTP_ACCEPTED;
        }
        return $result;
    }
}
