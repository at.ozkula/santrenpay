<?php
/**
 * Wali Santri Model
 * 24 July 2018
 */
class Wali_santri_model extends CI_Model
{
    public function check_anak($parameter)
    {
        $PesantrenDB   = $this->load->database('pesantren', true);
        $nis           = $parameter['nis'];
        $pesantren_id  = $parameter['pesantren_id'];
        $QueryListAnak = "SELECT * FROM anak a
                          LEFT JOIN pesantren p ON p.idpesantren = a.pesantren_id
                          WHERE a.nis = ? AND a.pesantren_id = ?";
        $CheckResult = $PesantrenDB->query($QueryListAnak, array($nis, $pesantren_id))->result_array();

        if (!empty($CheckResult)) {
            $result['result'] = $CheckResult[0];
            $result['status'] = REST_Controller::HTTP_OK;
        } else {
            $result['result'] = "No Record Show";
            $result['status'] = REST_Controller::HTTP_NO_CONTENT;
        }
        return $result;
    }
    public function list_anak($token)
    {
        $PesantrenDB = $this->load->database('pesantren', true);

        $QueryGetUserId  = "SELECT * FROM user WHERE token = ?";
        $ResultGetUserId = $this->db->query($QueryGetUserId, array($token))->result_array()[0];

        $QueryListAnak  = "SELECT * FROM user_fav WHERE user_id = ?";
        $ResultListAnak = $this->db->query($QueryListAnak, array($ResultGetUserId['iduser']))->result_array();

        if (!empty($ResultListAnak)) {
            $anakLISt = array();
            $listine  = array();
            for ($i = 0; $i < count($ResultListAnak); $i++) {
                $QueryListAnak = "SELECT * FROM anak a
                          LEFT JOIN pesantren p ON p.idpesantren = a.pesantren_id
                          WHERE a.idanak = ?";
                $CheckResult[] = $PesantrenDB->query($QueryListAnak, array($ResultListAnak[$i]['anak_id']))->result_array();
            }

            for ($CA = 0; $CA < count($CheckResult); $CA++) {
                $namaanak      = $CheckResult[$CA][0]['nama'];
                $nis           = $CheckResult[$CA][0]['nis'];
                $nama_santren  = $CheckResult[$CA][0]['nama_santren'];
                $pesantren_id  = $CheckResult[$CA][0]['pesantren_id'];
                $GetListAnak[] = array(
                    'idlistanak'   => $CA,
                    'nis'          => $nis,
                    'nama_anak'    => $namaanak,
                    'nama_santren' => $nama_santren,
                    'pesantren_id' => $pesantren_id,
                );
            }

            $result['result'] = $GetListAnak;
            $result['status'] = REST_Controller::HTTP_OK;

        } else {
            $result['result'] = "No Record Show";
            $result['status'] = REST_Controller::HTTP_NO_CONTENT;
        }
        return $result;

    }
    public function simpan_anak($parameter)
    {
        $PesantrenDB  = $this->load->database('pesantren', true);
        $nis          = $parameter['nis'];
        $token        = $parameter['token'];
        $pesantren_id = $parameter['pesantren_id'];

        $QueryGetUserId = "SELECT * FROM user
                          WHERE token = ?";
        $ResultGetUserId = $this->db->query($QueryGetUserId, array($token))->result_array();

        $QueryListAnak = "SELECT * FROM anak a
                          LEFT JOIN pesantren p ON p.idpesantren = a.pesantren_id
                          WHERE a.nis = ? AND a.pesantren_id = ?";
        $ResultListAnak = $PesantrenDB->query($QueryListAnak, array($nis, $pesantren_id))->result_array();

        $checkForNotDoubleEntry = "SELECT * FROM user_fav WHERE user_id = ? AND anak_id = ?";
        $ResultDoubleEntry      = $this->db->query($checkForNotDoubleEntry, array($ResultGetUserId[0]['iduser'], $ResultListAnak[0]['idanak']))->num_rows();
        if ($ResultDoubleEntry > 0) {
            $result['result'] = "Anak sudah Ada di Daftar List";
            $result['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        } else {
            $dataLog = array(
                'user_id' => $ResultGetUserId[0]['iduser'],
                'anak_id' => $ResultListAnak[0]['idanak'],
            );
            $this->db->insert('user_fav', $dataLog);

            $result['result'] = "Berhasil Menambahkan Daftar Anak";
            $result['status'] = REST_Controller::HTTP_OK;
        }
        return $result;
    }
    public function hapus_anak($parameter)
    {
        $PesantrenDB  = $this->load->database('pesantren', true);
        $nis          = $parameter['nis'];
        $token        = $parameter['token'];
        $pesantren_id = $parameter['pesantren_id'];

        $QueryGetUserId = "SELECT * FROM user
                          WHERE token = ?";
        $ResultGetUserId = $this->db->query($QueryGetUserId, array($token))->result_array();

        $QueryListAnak = "SELECT * FROM anak a
                          WHERE a.nis = ? AND a.pesantren_id = ?";
        $ResultListAnak = $PesantrenDB->query($QueryListAnak, array($nis, $pesantren_id))->result_array();

        $querySelectAnak  = "SELECT * FROM user_fav WHERE user_id  = ? AND anak_id = ?";
        $ResultSelectAnak = $this->db->query($querySelectAnak, array($ResultGetUserId[0]['iduser'], $ResultListAnak[0]['idanak']))->result_array();
        if (!empty($ResultSelectAnak)) {
            $queryDelete  = "DELETE FROM user_fav WHERE user_id = ? AND anak_id = ?";
            $ResultDelete = $this->db->query($queryDelete, array($ResultGetUserId[0]['iduser'], $ResultListAnak[0]['idanak']));

            $result['result'] = "Berhasil Menghapus Anak dari List";
            $result['status'] = REST_Controller::HTTP_OK;
        } else {
            $result['result'] = "Anak Tidak ada DiList";
            $result['status'] = REST_Controller::HTTP_UNAUTHORIZED;
        }

        return $result;

    }

}
