<?php
/**
 * Log Activity Inserted Here 6 Juni2018
 * Check Status Log via DB tabel status
 * Check Type Transaction via DB tabel transaction_type
1    Kirim Uang
2    Pembayaran
 */
class Log_model extends CI_Model
{
    public function _log_transaction($transaction_type, $amount, $sender, $status_id, $reason)
    {
        $transaction_id = time() . uniqid(mt_rand(), true);
        $dataLog        = array(
            'transaction_id'   => $transaction_id,
            'transaction_type' => $transaction_type,
            'amount'           => $amount,
            'nohp'             => $sender,
            'status_id'        => $status_id,
            'timelog'          => date('Y-m-d H:i:s'),
            'reason'           => $reason,
        );
        $this->db->insert('log_transaction', $dataLog);
        
    }
    public function decrypt($userlogin, $encrypted)
    {
        $key       = $userlogin . 'SantrenPay';
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
        return $decrypted;
    }

    public function encrypt($userlogin, $decrypted)
    {
        $key       = $userlogin . 'SantrenPay';
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $decrypted, MCRYPT_MODE_CBC, md5(md5($key))));
        return $encrypted;
    }

}
