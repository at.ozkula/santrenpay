<?php
/**
 *
 */
class Merchant_model extends CI_Model
{
    public function list_merchant()
    {
        $merchantRow = "SELECT * FROM merchant";
        $CheckResult = $this->db->query($merchantRow)->result_array();

        $result['result'] = $CheckResult;
        $result['status'] = REST_Controller::HTTP_OK;

        return $result;
    }
    public function list_merchant_kategori($idkategori)
    {

        $merchantRow = "SELECT * FROM merchant WHERE merchant_kategori_id = ?";
        $CheckResult = $this->db->query($merchantRow, array($idkategori))->result_array();

        $result['result'] = $CheckResult;
        $result['status'] = REST_Controller::HTTP_OK;

        return $result;
    }
    public function list_kategori()
    {
        $kategoriRow = "SELECT * FROM merchant_kategori";
        $CheckResult = $this->db->query($kategoriRow)->result_array();

        $result['result'] = $CheckResult;
        $result['status'] = REST_Controller::HTTP_OK;

        return $result;
    }

    public function list_event()
    {
        $eventRow = "SELECT idevent,event_name AS 'kategori',event_nama,merchant_id FROM sp_event
                     LEFT JOIN event_kategori ON event_kategori.ideventkategori = sp_event.event_kategori_id";
        $CheckResult = $this->db->query($eventRow)->result_array();

        $result['result'] = $CheckResult;
        $result['status'] = REST_Controller::HTTP_OK;

        return $result;
    }
    function list_event_donasi(){
        
    }
    public function event_merchant($merchant_id)
    {
        $merchantRow = "SELECT * FROM sp_event WHERE merchant_id = ?";
        $CheckResult = $this->db->query($merchantRow, array($merchant_id))->result_array();
        $CheckRow    = $this->db->query($merchantRow, array($merchant_id))->num_rows();

        if ($CheckRow > 0) {
            $result['result'] = $CheckResult;
            $result['status'] = REST_Controller::HTTP_OK;
        } else {
            $result['result'] = "No Record Show";
            $result['status'] = REST_Controller::HTTP_NO_CONTENT;
        }

        return $result;
    }
    public function event_detail($event_id)
    {
        $merchantCheckKategori = "SELECT event_kategori_id FROM sp_event WHERE idevent = ?";
        $getKategori           = $this->db->query($merchantCheckKategori, array($event_id))->result_array();

        if (!empty($getKategori)) {
            $checkGetKategori = $getKategori[0]['event_kategori_id'];
        } else {
            $checkGetKategori = 'nokategori';
        }

        if ($checkGetKategori == 1) {
            // Pembayaran
            $geteventDetail = "SELECT
                                    idevent,ideventkategori,event_name AS 'kategori',event_nama,event_deskripsi,event_value AS 'nominal',merchant_id,nohp
                                     FROM sp_event
                                    LEFT JOIN event_detail ON sp_event.idevent = event_detail.event_id
                                    LEFT JOIN event_kategori ON sp_event.event_kategori_id = event_kategori.ideventkategori
                                    LEFT JOIN event_payment ON sp_event.idevent = event_payment.event_id
                                    LEFT JOIN merchant ON sp_event.merchant_id = merchant.idmerchant
                                    LEFT JOIN user ON merchant.user_id = user.iduser
                                   WHERE idevent = ?";
            $returnEventDetail = $this->db->query($geteventDetail, array($event_id))->result_array();
            $CheckRow          = $this->db->query($geteventDetail, array($event_id))->num_rows();
        } else if ($checkGetKategori == 2) {
            // Donasi
            $geteventDetail = "SELECT
                                    idevent,ideventkategori,event_name AS 'kategori',event_nama,event_deskripsi,event_value AS 'nominal',merchant_id,nohp
                                   FROM sp_event
                                    LEFT JOIN event_detail ON sp_event.idevent = event_detail.event_id
                                    LEFT JOIN event_kategori ON sp_event.event_kategori_id = event_kategori.ideventkategori
                                    LEFT JOIN event_donasi ON sp_event.idevent = event_donasi.event_id
                                    LEFT JOIN merchant ON sp_event.merchant_id = merchant.idmerchant
                                    LEFT JOIN user ON merchant.user_id = user.iduser
                                   WHERE idevent = ?";
            $returnEventDetail = $this->db->query($geteventDetail, array($event_id))->result_array();
            $CheckRow          = $this->db->query($geteventDetail, array($event_id))->num_rows();
        } else {
            $CheckRow = 0;
        }

        if ($CheckRow > 0) {
            $result['result'] = $returnEventDetail[0];
            $result['status'] = REST_Controller::HTTP_OK;
        } else {
            $result['result'] = "No Record Show";
            $result['status'] = REST_Controller::HTTP_NO_CONTENT;
        }

        return $result;
    }
}
