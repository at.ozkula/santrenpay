<!DOCTYPE html>
<html lang="en">
  <head>
    <?php $this->load->view('global/header') ?>
  </head>

  <body class="login">
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" action="<?php echo base_url(); ?>index.php/home/login"> 
              <h1>Login Backend</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" name="username" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" name="password" />
              </div>
              <div>
                <button class="btn btn-default submit btn-block" >Log in</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                
                <div class="clearfix"></div>
                <br />
              </div>
            </form>
          </section>
        </div>

        
      </div>
    </div>
  </body>
</html>
