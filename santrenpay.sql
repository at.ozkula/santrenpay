-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `event_detail`;
CREATE TABLE `event_detail` (
  `ideventdetail` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `event_deskripsi` text NOT NULL,
  PRIMARY KEY (`ideventdetail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `event_donasi`;
CREATE TABLE `event_donasi` (
  `ideventdonasi` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `event_value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ideventdonasi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `event_kategori`;
CREATE TABLE `event_kategori` (
  `ideventkategori` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`ideventkategori`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `event_kategori` (`ideventkategori`, `event_name`) VALUES
(1,	'Pembayaran'),
(2,	'Donasi');

DROP TABLE IF EXISTS `event_payment`;
CREATE TABLE `event_payment` (
  `ideventpayment` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `event_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`ideventpayment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `log_transaction`;
CREATE TABLE `log_transaction` (
  `idlogtransaction` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(16) NOT NULL,
  `transaction_type` int(2) NOT NULL,
  `amount` varchar(10) NOT NULL DEFAULT '',
  `nohp` varchar(15) NOT NULL DEFAULT '',
  `status_id` int(2) NOT NULL,
  `timelog` datetime NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`idlogtransaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `merchant`;
CREATE TABLE `merchant` (
  `idmerchant` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_kategori_id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL DEFAULT '',
  `deskripsi` text NOT NULL,
  `alamat` text NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`idmerchant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `merchant` (`idmerchant`, `merchant_kategori_id`, `nama`, `deskripsi`, `alamat`, `user_id`) VALUES
(1,	1,	'Pesantren ABC',	'Ini adalah Deskripsi Pesantren Dari Merchant',	'Alamat PEsantren\n',	1),
(2,	4,	'AOC',	'Agent Of Change',	'Alamat AOC',	2);

DROP TABLE IF EXISTS `merchant_kategori`;
CREATE TABLE `merchant_kategori` (
  `idkategori` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`idkategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `merchant_kategori` (`idkategori`, `kategori`) VALUES
(1,	'Pesantren'),
(2,	'MiniMarket'),
(3,	'Kantin'),
(4,	'Donasi / Pil Rejeki');

DROP TABLE IF EXISTS `sp_event`;
CREATE TABLE `sp_event` (
  `idevent` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_kategori_id` int(11) NOT NULL,
  `event_nama` varchar(50) NOT NULL DEFAULT '',
  `merchant_id` int(11) NOT NULL,
  PRIMARY KEY (`idevent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sp_event` (`idevent`, `event_kategori_id`, `event_nama`, `merchant_id`) VALUES
(1,	0,	'',	0);

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(50) NOT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `status` (`idstatus`, `status_name`) VALUES
(1,	'User Baru Belum Diverifikasi'),
(2,	'Transaksi Gagal'),
(3,	'Transaksi Gagal Saldo Kurang'),
(4,	'Menerima Dana'),
(5,	'Mengirim Dana');

DROP TABLE IF EXISTS `transaction_type`;
CREATE TABLE `transaction_type` (
  `idtransactiontype` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_name` varchar(50) NOT NULL,
  PRIMARY KEY (`idtransactiontype`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `transaction_type` (`idtransactiontype`, `transaction_name`) VALUES
(1,	'Kirim Uang'),
(2,	'Pembayaran');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `nohp` varchar(15) NOT NULL,
  `balance` int(10) NOT NULL,
  `token` varchar(60) NOT NULL,
  `authpin` int(6) NOT NULL,
  `loginTime` datetime NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`iduser`, `nohp`, `balance`, `token`, `authpin`, `loginTime`) VALUES
(1,	'081331005514',	109361,	'4wwggg0w4ksswkowc4kkscssc80skgw0g0s0g0c0',	123456,	'2018-07-05 15:17:31'),
(2,	'081313',	1036000,	'wocsk4ssg0o8kokosgg4k88o0sgskcgwokwoc4k4',	134511,	'2018-06-29 17:12:41'),
(3,	'082222222222',	1662500,	'8cgosw040k4w08c4sskw0ggggwg8wswks8cwk8ws',	123456,	'2018-07-19 11:20:45'),
(4,	'08123456789',	1000000,	'ckk4o8so484w4sccs044sso800gsk4koo44socgs',	123456,	'2018-06-29 17:10:05'),
(5,	'08111142124',	949000,	'occs0ocgskwwog4csw0wo8wscosww4kk4gg8ow0w',	898989,	'2018-07-13 09:48:44'),
(6,	'0812121212',	0,	'0c404ggoc44kw88w84cck0cgscwgcw8sosk0sooc',	123456,	'0000-00-00 00:00:00'),
(7,	'08232323',	0,	'o0c8ks4csg0kwswg0wccg040kk4w40wwg44oc4w8',	123456,	'0000-00-00 00:00:00'),
(8,	'0811',	0,	'sgww000k80kc48swsc08sg48kkwkog848gw4gg4k',	643112,	'0000-00-00 00:00:00'),
(9,	'081313131',	0,	'4gkswgkowkc4c4w4w4wo84sw4osgg484kossgs4s',	0,	'0000-00-00 00:00:00'),
(10,	'082221801234',	0,	'4og4o8wowok4kkwgw84wws8gcocgs8ocoowook4w',	123456,	'0000-00-00 00:00:00'),
(11,	'082323232323',	0,	'gw0k4cc4ko80g8k8c8ow0wkw48cc88wgo8ck8sog',	0,	'0000-00-00 00:00:00'),
(12,	'082221801235',	0,	'44kww48ocw88gco4gwoowsgs4ck0wo0kgo0wsc0k',	0,	'0000-00-00 00:00:00'),
(13,	'082221801239',	0,	'ggc0kswcwcsgg8gcgcokk0ococ8os0w804s0kgco',	0,	'0000-00-00 00:00:00'),
(14,	'0814141414',	0,	'88gsk0osokwg448ggk0g04ow48kg0c4go8s4gk0k',	0,	'0000-00-00 00:00:00'),
(15,	'0817171717',	0,	'wowwkc0kwgcg4g888sgw0g0ko8k00s4sw8gcos8s',	0,	'0000-00-00 00:00:00'),
(16,	'081122',	0,	'k8go08owwowwks84s44gc0k48o8sck488w8cckks',	0,	'0000-00-00 00:00:00'),
(17,	'081122211',	0,	'4kk000cg4k0o0kggw8wogcg0wsw8g8wco4s044sg',	283723,	'0000-00-00 00:00:00'),
(18,	'082221801221',	0,	'048wgcg804s8kow4owcoso0s8cc8c4ck4kco4kwk',	0,	'0000-00-00 00:00:00'),
(19,	'0824242425',	0,	'4oskwk4844c0kwgokcoww0sk0wgw4sw8kkcggog4',	0,	'0000-00-00 00:00:00'),
(20,	'088888888888',	0,	'wo4w0ww4s88wk8ss40cggkokw0sg4s04gc4c0kgg',	0,	'0000-00-00 00:00:00'),
(21,	'0808080808',	0,	'cg840g0cs4wwsgcco8o8csk8o8goc8wgw44soows',	0,	'0000-00-00 00:00:00'),
(22,	'0898989898',	0,	'o4oksgc0o4gsgo4ws4ccgkkk4w4cc8840cws4okk',	0,	'0000-00-00 00:00:00'),
(23,	'0878787878',	0,	'4owcg0g8wk4go4cws0kg8s0wck4os4g8k48cg4sw',	0,	'0000-00-00 00:00:00'),
(24,	'0825252525',	0,	'40okowksgk04wsogcg4kkck4g00sggk8kg0osog4',	0,	'0000-00-00 00:00:00'),
(25,	'0854649865',	0,	'ogsoo0o48k8kw888cwokso40g4c4oc0cgscwkwwo',	0,	'0000-00-00 00:00:00'),
(26,	'082221891221',	0,	'k8gokgccsc0ww4sw4scs44ok0cgs80kogsw8g0ko',	0,	'0000-00-00 00:00:00'),
(27,	'0825452536',	0,	'8c4w0w0o4o0ow8k4wwock0kwk0ww0gw0s4gw0c08',	0,	'0000-00-00 00:00:00'),
(28,	'08649785436',	0,	'o0oowoosow8sogcksgcc0k4c40oowgsgsc804gw8',	0,	'0000-00-00 00:00:00'),
(29,	'08649785433',	0,	'044w4cgk8ggoko4wwosk8wowsww0k0gwcoggsc0o',	0,	'0000-00-00 00:00:00'),
(30,	'0898764523',	0,	'kscsk8cg0k0cwgks8ok8c0c408ocks0gocogwgk8',	0,	'0000-00-00 00:00:00'),
(31,	'0832145852',	0,	'4w4owcg0008kccgsowc0w0g4so4gww0g444ggoc0',	0,	'0000-00-00 00:00:00'),
(32,	'0814752369',	0,	'8owk4kk8w4kc4s8swcggcs4kk8scowk4ksc0ogk8',	0,	'0000-00-00 00:00:00'),
(33,	'08147523690',	0,	'c8cokookwss0gkkg4048oow00cgwokk88so0kc8c',	0,	'0000-00-00 00:00:00'),
(34,	'08140523690',	0,	'00okkgs8o8oowg48wc0co0o8g04kwgk48c0g44gg',	0,	'0000-00-00 00:00:00'),
(35,	'08794613569',	0,	's04o0gokw8sggwg0oo40cwgg8g8k40ccw880g4gw',	0,	'0000-00-00 00:00:00'),
(36,	'08704613569',	0,	'wsckc4gk0csskwgkskwko0sco0woswg04wcko4s0',	123456,	'0000-00-00 00:00:00'),
(37,	'0864976853',	0,	'84co8oc88cw0swk8cow4sok8sogc80wooo4gsgs0',	0,	'0000-00-00 00:00:00'),
(38,	'0864978653',	0,	'wgg8wwk8c8w0k80skoc0sg40840k8480sooooccs',	0,	'0000-00-00 00:00:00'),
(39,	'084653197',	0,	'4c4wc00gw48cosscksc8cokwgowow4ww0go808so',	0,	'0000-00-00 00:00:00'),
(40,	'08794658963',	0,	'08ggkck4kcc8o8o4osok40gkgg4kk4g4wosgg4oc',	0,	'0000-00-00 00:00:00'),
(41,	'0852589635',	0,	'c4wccg84oow8gcc40cgo840w4gscg4k4skc8okoc',	0,	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `user_detail`;
CREATE TABLE `user_detail` (
  `iduser_detail` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `verifyemail` varchar(11) NOT NULL,
  `otp` int(6) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`iduser_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_detail` (`iduser_detail`, `user_id`, `nama`, `password`, `email`, `verifyemail`, `otp`, `status`) VALUES
(1,	1,	'RozaqIN',	'erereere',	'',	'',	0,	3),
(2,	2,	'Diduakan',	'erereere',	'',	'',	0,	3),
(3,	3,	'Arga',	'erereere',	'',	'',	0,	3),
(4,	4,	'Coba',	'erereere',	'',	'',	0,	3),
(5,	5,	'Hario Baskoro Basoek',	'erereere',	'',	'',	0,	3),
(6,	6,	'Register',	'erereere',	'',	'',	0,	3),
(7,	7,	'Test',	'erereere',	'',	'',	0,	3),
(8,	8,	'Rozaq Dua Tes',	'1233123123',	'at.ozkula@gmail.com',	'sgww00',	0,	4),
(9,	9,	'Aku',	'',	'aku@gmail.com',	'4gkswg',	0,	1),
(10,	10,	'John Doe',	'pasword',	'johndoe@mail.com',	'success',	0,	4),
(11,	11,	'Dalam',	'',	'dalam@mail.com',	'gw0k4c',	0,	1),
(12,	12,	'John Doe1',	'',	'johndoae@mail.com',	'44kww4',	0,	1),
(13,	13,	'John Doe1',	'',	'johndoaae@mail.com',	'ggc0ks',	0,	1),
(14,	14,	'Ini',	'',	'inu@mail.com',	'88gsk0',	0,	1),
(15,	15,	'Aku',	'',	'aku@mailll.com',	'wowwkc',	0,	1),
(16,	16,	'Inul',	'',	'inulMilikmu@ya.com',	'k8go08',	0,	1),
(17,	17,	'Inuliaa',	'passwordinidisi in lhoo',	'inulMilikmaaau@ya.com',	'success',	0,	4),
(18,	18,	'John Doe1',	'',	'johndoaaae@mail.com',	'048wgc',	0,	1),
(19,	19,	'Dalam',	'',	'dalamuu@mail.com',	'4oskwk',	0,	1),
(20,	20,	'Semua',	'',	'semua@mail.com',	'wo4w0w',	0,	1),
(21,	21,	'Transfer',	'',	'transfer@mail.com',	'cg840g',	0,	1),
(22,	22,	'Coba',	'',	'coba@mail.com',	'o4oksg',	0,	1),
(23,	23,	'Brksj',	'',	'ha@majk.com',	'success',	0,	2),
(24,	24,	'Aku dah',	'',	'Ghjki@hjjj.com',	'40okow',	0,	1),
(25,	25,	'Hsjsj',	'',	'Jskks@jdjdj.com',	'ogsoo0',	0,	1),
(26,	26,	'John Doe1',	'',	'johndoaa0ae@mail.com',	'k8gokg',	0,	1),
(27,	27,	'Gjjghj',	'',	'Hhjkk@hikbh.com',	'success',	0,	2),
(28,	28,	'Jsjjks',	'',	'Hjjsj@jjks.com',	'o0oowo',	0,	1),
(29,	29,	'Jsjjks',	'',	'Hjgjsj@jjks.com',	'success',	0,	2),
(30,	30,	'arduino',	'',	'argdk@mail.com',	'success',	0,	2),
(31,	31,	'The',	'',	'dthgu@mdguu.com',	'4w4owc',	0,	1),
(32,	32,	'Hi',	'',	'sfgyy@ghui.com',	'8owk4k',	0,	1),
(33,	33,	'Hi',	'',	'sfgygy@ghui.com',	'c8coko',	0,	1),
(34,	34,	'Hi',	'anjay',	'sfghhhhygy@ghui.com',	'success',	0,	3),
(35,	35,	'Agus',	'',	'ajjjs@jsjks.com',	's04o0g',	0,	1),
(36,	36,	'Agus',	'brother',	'akjjs@jsjks.com',	'success',	0,	4),
(37,	37,	'Grs',	'',	'hsj@kdk.com',	'84co8o',	0,	1),
(38,	38,	'Sjjjs',	'',	'jjsj@jjjs.com',	'wgg8ww',	0,	1),
(39,	39,	'Djjskis',	'',	'Sjjkis@jjsj.ck',	'4c4wc0',	0,	1),
(40,	40,	'The only',	'',	'jsjiis@jsjjs.com',	'08ggkc',	0,	1),
(41,	41,	'Hjsjjs',	'',	'jjsjjs@jdjjs.dk',	'c4wccg',	0,	1);

-- 2018-07-19 07:35:24
